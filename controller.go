package hxgo

// controller object
type Controller struct {
	R *Request
	Rs *Response
	Input *Input
	View *View
}

// new controller func
func (this *Controller) New(w *Response, r *Request, v *View) {
	this.R = r
	this.Rs = w
	this.Input = this.R.Input()
	this.View = v
}

// init controller func, empty for basic controller struct
func (this *Controller) Init() {

}

// default controller called method
func (this *Controller) Index() {
	this.Error(404, "Page Not Found")
}

// controller done
func (this *Controller) Done(content string) {
	this.Rs.Content = content
	this.Rs.Done()
}

// redirect url
func (this *Controller) Redirect(url string) {
	this.Rs.Redirect(url)
}

// display view template and done
func (this *Controller) Display(t string, data map[string]interface {}, nest []string) error {
	str, e := this.View.Render(t, data, nest)
	if e != nil {
		return e
	}
	this.Done(str)
	return nil
}

// do error to error handler
func (this *Controller) Error(status int, message string) {
	this.Rs.Error(status, message)
}

// show json response and done
func (this *Controller) Json(data interface {}) error {
	return this.Rs.Json(data)
}
